# Derk

A dark theme based off of w3.css's blog template. (by default, but customizable)

## Installation

### Install
```bash
git clone https://gitlab.com/cyberronin/hexo-theme-derk themes/derk
```

### Enable
In your main `_config.yml`, change your `theme` setting to `derk`.

### Update
```bash
cd themes/derk
git pull
```

## Configuration
```yml
#this is what displays on top of the blog's index
welcome: "Welcome to the blog of "

#author info
author:
  gravatar: "email for gravatar"

#these widgets are enabled by default
widgets:
  - categories
  - tagcloud
  - recent-posts

#links to the author's sites.
links:
  github: https://github.com/<user>
  gitlab: https://gitlab.com/<user>
  twitter: https://twitter.com/<user>

#colors
ui:
  primary: "#282828"
  secondary: "#4d4d4d"
  accent: "black"
  text: "white"
```

- **welcome**: This text is displayed on the blog's index.
- **author**: Author's email that has a gravatar associated.
- **widgets**: Widgets displaying on the side of the index page.
- **links**: Some social media links. (the three on by default are the only ones I support so far)
- **ui**: Customizable colors for the UI.

## Features

### Responsive
Derk is fully responsive and looks quite nice on mobile!

### Customizable Colors
Derk's UI colors are customizable via the `_config.yml` file. 

### Sidebar
The sidebar is on the left side and will be hidden on mobile, but there will be a hamburger icon to get it to show.

Derk's sidebar contains:  
- The author's name
- Gravatar image
- Navigation
- Author's social links
- Table of Contents on posts
